from manimlib import *

class ChallengeSolve(Scene):
    def construct(self):
        filhos_e_produtos = TexText('F = 3 e P = 36')
        self.play(Write(filhos_e_produtos))
        self.wait()
        self.play(filhos_e_produtos.animate.move_to(TOP*0.8+LEFT*5))
        self.wait()
        #============== FACTOR 36 ==============# 
        title = TexText('Fatorar 36 e encontrar trios').move_to(BOTTOM*0.8+LEFT*4)
        main_number = Tex('36').move_to(UP*3)
        numbers = VGroup(
            Tex('1',',','1',',','36'), 
            Tex('1',',','2',',','18'), 
            Tex('1',',','3',',','12'), 
            Tex('1',',','4',',','9'), 
            Tex('1',',','6',',','6'), 
            Tex('2',',','2',',','9'), 
            Tex('2',',','3',',','6'), 
            Tex('3',',','3',',','4'), 
        )
        numbers.arrange(DOWN, buff=MED_SMALL_BUFF)
        bar = Line().move_to(UP*2.7)
        self.play(Write(title))
        self.play(Write(main_number))
        self.play(ShowCreation(bar))
        self.play(Write(numbers),run_time=3)
        self.wait()
        self.play(FadeOut(main_number), FadeOut(bar))
        self.play(numbers.animate.move_to(RIGHT*5))
        self.wait()
        #============== Sum each trio ==============# 
        self.play(Transform(title, TexText('Somar o nº trio').move_to(BOTTOM*0.8+LEFT*4)))
        n = TexText('n = 1')
        sums = VGroup(
            Tex('38'),
            Tex('21'),
            Tex('16'),
            Tex('14'),
            Tex('13'),
            Tex('13'),
            Tex('11'),
            Tex('10'),
        )
        sums.arrange(DOWN, buff=0.34)
        sums.move_to(RIGHT*3.5)
        self.play(Write(n))
        for row,number in enumerate(numbers):
            self.play(Transform(n, TexText('n = '+str(row+1))))
            self.play(Transform(number, number.set_color(RED)))
            self.play(Write(sums[row]))
            self.play(Transform(number, number.set_color(WHITE)))
        self.play(FadeOut(n))
        #============== Test1 ==============# 
        self.play(Transform(title, TexText('Há resultado de soma repetido?').move_to(BOTTOM*0.8+LEFT*3.5)))
        answer = TexText('SIM', color=GREEN_SCREEN)
        self.wait()
        self.play(FadeIn(answer))
        self.play(
            Transform(numbers[4], numbers[4].set_color(GREEN_SCREEN)),
            Transform(sums[4], sums[4].set_color(GREEN_SCREEN)),
            Transform(numbers[5], numbers[5].set_color(GREEN_SCREEN)),
            Transform(sums[5], sums[5].set_color(GREEN_SCREEN)),
        )
        self.wait()
        for i in range(8):
            if i != 4 and i != 5:
                self.play(FadeOut(sums[i]),run_time=0.1)
                self.play(FadeOut(numbers[i]),run_time=0.1)
        self.play(FadeOut(answer))
        self.wait()
        #============== Test1 ==============# 
        self.play(Transform(title, TexText('Somente uma opção tem um número maior que o resto?').move_to(BOTTOM*0.8)))
        self.play(FadeIn(answer))
        self.wait(3)
        self.play(FadeOut(VGroup(numbers[4],sums[4])))
        self.wait()
        final = TexText('As idades dos filhos são 2, 2 e 9 anos')
        self.play(FadeOut(VGroup(title, answer, filhos_e_produtos))) 
        self.play(TransformMatchingShapes(VGroup(sums[5],numbers[5]),final))
        self.wait(3) 
        self.play(FadeOut(final))

rules_red = [ 
'Castilho recomenda o livro "Algoritmos: Teoria e Prática"',
'VRI fica à esquerda do C3SL',
'Quem fica no VRI pesquisa na área da Robótica',
'Castilho fica ao lado do VRI',
'Quem recomenda o livro "Organização e Arquitetura de Computadores" fica ao lado do C3SL'
]
rules_python = [
'Quem usa python recomenda o livro "Artificial Intelligence: A Modern Approach"',
'Quem usa Go fica ao lado de quem recomenda o livro "Artificial Intelligence: A Modern Approach"',
'Quem usa Go é vizinho de quem pesquisa na área de Engenharia de Software'
]
rules_pink = [
'Quem fica no FAES usa Java'
]
rules_todt = ['Todt usa C']
rules_pascal = ['Quem usa Pascal pesquisa na área da Computação Científica']
rules_zanata = ['Zanata trabalha com pesquisa na área de Arquitetura de Computadores']
class AbstractionSolve(Scene): 
    def construct(self):
        title = TexText('A melhor maneira de resolver o problema é de um modo abstrato').to_edge(UP)
        self.play(Write(title))
        self.wait(2)
        self.play(Transform(title, TexText('Vejamos a grade como um quebra cabeça comum').to_edge(UP)))
        grid = self.grid_gen()
        grid = self.get_group_grid(grid)
        grid.move_to(ORIGIN)
        self.play(ShowCreation(grid))
        self.wait()
        
        self.play(Transform(title, TexText('Assim, pelas dicas, sabe-se que duas peças são fixas').to_edge(UP)))

        self.play(grid[5].animate.set_fill(WHITE,opacity=1))
        self.play(grid[12].animate.set_fill(WHITE,opacity=1))
        self.wait()

        self.play(grid.animate.to_edge(RIGHT))

        self.play(Transform(title, TexText('Cada regra diz a posição relativa entre as peças').to_edge(UP)))
        self.wait(3)
        self.play(Transform(title, TexText('Por exemplo:').to_edge(UP)))
        rules_texts = VGroup()
        for r in rules_python:
            rules_texts.add(TexText(r).scale(0.7))
        rules_texts.arrange(DOWN,buff=MED_SMALL_BUFF)
        self.play(FadeOut(grid,shift=RIGHT))
        self.play(Write(rules_texts))
        
        pieces_python = VGroup()

        n = Square(side_length=0.7).set_fill(ORANGE, opacity=1)
        pieces_python.add(n)
        n = Square(side_length=0.7).next_to(pieces_python[0],DOWN,buff=0).set_fill(ORANGE, opacity=1)
        pieces_python.add(n)
        n = Square(side_length=0.7).next_to(pieces_python[0],LEFT,buff=0).set_fill(ORANGE,opacity=0.5)
        pieces_python.add(n)
        n = Square(side_length=0.7).next_to(pieces_python[0],RIGHT,buff=0).set_fill(ORANGE,opacity=0.5)
        pieces_python.add(n)
        n = Square(side_length=0.7).next_to(pieces_python[3],RIGHT+UP,buff=0).set_fill(ORANGE,opacity=0.5)
        pieces_python.add(n)
        n = Square(side_length=0.7).next_to(pieces_python[2],LEFT+UP,buff=0).set_fill(ORANGE,opacity=0.5)
        pieces_python.add(n)
        self.wait(3)
        self.play(FadeOut(rules_texts))
        self.play(
                ShowCreation(pieces_python),
                Transform(title, TexText('Peças com opacidade mais fraca podem alterar suas posições entre um lado e outro, pois a regra não deixa clara sua exata localização').scale(0.7).to_edge(UP))
        )
        self.wait(5)

        self.play(
            Transform(
                title,
                TexText('Com isso, todas as peças formadas devem ser algo como o seguinte:').to_edge(UP)
            )
        )

        pieces_red = VGroup()
        n = Square(side_length=0.7).set_fill(RED,opacity=1)
        pieces_red.add(n)
        n = Square(side_length=0.7).next_to(pieces_red[0],LEFT,buff=0).set_fill(RED, opacity=1)
        pieces_red.add(n)
        n = Square(side_length=0.7).next_to(pieces_red[1],DOWN,buff=0.7).set_fill(RED, opacity=1)
        pieces_red.add(n)
        n = Square(side_length=0.7).next_to(pieces_red[0],DOWN,buff=0).set_fill(RED, opacity=0.5)
        pieces_red.add(n)
        n = Square(side_length=0.7).next_to(pieces_red[1],DOWN+LEFT,buff=0).set_fill(RED, opacity=0.5)
        pieces_red.add(n)
        n = Square(side_length=0.7).next_to(pieces_red[3],DOWN,buff=1.4).set_fill(RED, opacity=0.5)
        pieces_red.add(n)
        n = Square(side_length=0.7).next_to(pieces_red[4],DOWN,buff=1.4).set_fill(RED, opacity=0.5)
        pieces_red.add(n)
        n = Square(side_length=0.7).next_to(pieces_red[5],RIGHT,buff=0).set_fill(MAROON, opacity=0.5)
        pieces_red.add(n)
        n = Square(side_length=0.7).next_to(pieces_red[5],LEFT,buff=0).set_fill(MAROON, opacity=0.5)
        pieces_red.add(n)
        pieces_red.move_to(ORIGIN)

        pieces_faes = VGroup()
        n = Square(side_length=0.7).set_fill(PINK,opacity=1)
        pieces_faes.add(n)
        n = Square(side_length=0.7).next_to(pieces_faes[0],DOWN,buff=1.4).set_fill(PINK,opacity=1)
        pieces_faes.add(n)
        n = Square(side_length=0.7).next_to(pieces_faes[1],DOWN+RIGHT,buff=0).set_fill(PINK,opacity=1)
        pieces_faes.add(n)
        pieces_faes.move_to(DOWN*2)
        pieces_faes.to_edge(RIGHT)

        pieces_todt = VGroup()
        n = Square(side_length=0.7).set_fill(TEAL_E, opacity=1)
        pieces_todt.add(n)
        n = Square(side_length=0.7).set_fill(TEAL_E, opacity=1).next_to(pieces_todt[0],DOWN,buff=0.7)
        pieces_todt.add(n)
        pieces_todt.move_to(UP*1.5)
        pieces_todt.to_edge(RIGHT)

        pieces_pascal = VGroup()
        n = Square(side_length=0.7).set_fill(GREEN, opacity=1) 
        pieces_pascal.add(n)
        n = Square(side_length=0.7).set_fill(GREEN, opacity=1).next_to(pieces_pascal[0],DOWN,buff=0)
        pieces_pascal.add(n)
        pieces_pascal.move_to(UP)
        pieces_pascal.to_edge(LEFT)

        pieces_zanata = VGroup()
        n = Square(side_length=0.7).set_fill(GOLD, opacity=1)
        pieces_zanata.add(n)
        n = Square(side_length=0.7).set_fill(GOLD, opacity=1).next_to(pieces_zanata[0], DOWN, buff=0)
        pieces_zanata.add(n)
        pieces_zanata.move_to(RIGHT*2.5)

        pieces_liamf = VGroup()
        n = Square(side_length=0.7).set_fill(BLUE_D, opacity=1)
        pieces_liamf.add(n)
        n = Square(side_length=0.7).set_fill(BLUE_D,opacity=1).next_to(pieces_liamf[0],DOWN,buff=0)
        pieces_liamf.add(n)

        pieces_liamf.move_to(LEFT*2+UP)

        self.play(pieces_python.animate.to_edge(DOWN+LEFT))
        self.play(ShowCreation(pieces_pascal))
        self.play(ShowCreation(pieces_todt))
        self.play(ShowCreation(pieces_liamf))
        self.play(ShowCreation(pieces_red))
        self.play(ShowCreation(pieces_zanata))
        self.play(ShowCreation(pieces_faes))

        self.wait()
        self.play(Transform(
            title,
            TexText('Então, sabendo em que posição colocar as peças, basta montar um quebra cabeça relativamente fácil').scale(0.8).to_edge(UP)
        ))
        self.play(FadeOut(VGroup(pieces_faes,pieces_liamf,pieces_todt,pieces_pascal,pieces_python,pieces_zanata)))
        self.play(pieces_red.animate.to_edge(LEFT))
        grid.move_to(ORIGIN)
        self.play(FadeIn(grid, shift=LEFT))
        self.wait(2) 
        self.play(Transform(
            title,
            TexText('Note que um algoritmo de computador poderia resolver esse problema recursivamente, posicionando cada peça na primeira possibilidade, e então mudando sua localização caso não seja possível seguir inserindo peças naquela posição').scale(0.7).to_edge(UP)
        ))
        self.play(pieces_red.animate.move_to(ORIGIN+RIGHT*1.05))
        self.play(FadeOut(pieces_red[7]))
        self.play(FadeIn(pieces_python))
        self.play(pieces_python.animate.move_to(ORIGIN+DOWN*0.7))
        self.play(FadeOut(pieces_red[4]),FadeOut(pieces_red[6]))
        self.play(pieces_red[3].animate.set_fill(RED,opacity=1))
        self.play(pieces_red[5].animate.set_fill(RED,opacity=1))
        self.play(pieces_red[8].animate.set_fill(RED,opacity=1))
        self.play(FadeIn(pieces_faes))
        self.play(pieces_faes.animate.move_to(ORIGIN+LEFT*1.05))
        self.play(FadeIn(pieces_pascal))
        self.play(pieces_pascal.animate.move_to(ORIGIN+RIGHT*1.4+DOWN*0.35))
        self.play(FadeOut(VGroup(pieces_python[3],pieces_python[4])))
        self.play(pieces_python[2].animate.set_fill(ORANGE,opacity=1))
        self.play(pieces_python[5].animate.set_fill(ORANGE,opacity=1))
        self.play(FadeIn(pieces_todt))
        self.play(pieces_todt.animate.move_to(ORIGIN+RIGHT*0.7))
        self.play(FadeIn(pieces_zanata))
        self.play(pieces_zanata.animate.move_to(ORIGIN+LEFT*0.7+UP*0.35))
        self.play(FadeIn(pieces_liamf))
        self.play(pieces_liamf.animate.move_to(ORIGIN+UP*1.05))

        self.play(Transform(
            title,
            TexText('Note que só restaram dois lugares vagos, e cada um é de uma categoria diferente, portanto pode-se seguramente deduzir o resto').scale(0.8).to_edge(UP)
        ))
        self.play(grid[1].animate.set_fill(YELLOW_E,opacity=1))
        self.play(grid[20].animate.set_fill(YELLOW_E,opacity=1))
        self.wait(3)
        self.play(Transform(
            title,
            TexText('Então, ao removermos nossa camada de abstração, obtemos a resposta').scale(0.8).to_edge(UP)
        ))

        final_texts = [
        'FAES','HiPES','LIAMF','VRI','C3SL',
        'Letícia','Zanata','Fabiano','Todt','Castilho',
        'E.Softw.', 'Arq.','IA','Robótica','Comp.C.',
        'Java','Go','Python','C','Pascal',
        'Inf.S.', 'TheArt', 'AI','Org.Arq.','Alg.',
        ]
        self.wait(3)
        self.play(FadeOut(title))
        self.play(FadeOut(VGroup(pieces_red,pieces_faes,pieces_todt,pieces_liamf,pieces_pascal,pieces_python,pieces_zanata)), grid[1].animate.set_fill(WHITE,opacity=0),grid[20].animate.set_fill(WHITE,opacity=0),grid[5].animate.set_fill(WHITE,opacity=0),grid[12].animate.set_fill(WHITE,opacity=0))
        self.play(grid.animate.scale(2))
        texts = VGroup()
        for i,square in enumerate(grid):
            text = TexText(final_texts[i])
            text.move_to(square.get_center()).scale(0.7)
            texts.add(text)
        self.play(Write(texts))
        self.wait(8)
        self.play(FadeOut(VGroup(grid,texts)))
    def grid_gen(self,size=5):
        grid = []
        columns = []
        columns.append(Square(side_length=0.7))
        for i in range(0,size-1):
           columns.append(Square(side_length=0.7).next_to(columns[i],RIGHT,buff=0))
        grid.append(columns)
        for i in range(size-1):
            prevrow = grid[i]
            new_row = []
            for square in prevrow:
                below = Square(side_length=0.7)
                below.next_to(square,DOWN,buff=0)
                new_row.append(below)
            grid.append(new_row)
        return grid 
    def get_group_grid(self,grid):
        squares = []
        for row in grid:
            for square in row:
                squares.append(square)
        return VGroup(*squares)
