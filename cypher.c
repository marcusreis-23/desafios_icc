#include <stdio.h>
#include <string.h>

int find_char(char search, const char* arr);

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("Obviamente, precisamos de uma frase...\n");
        return 1;
    }
    const char* vogais = "AEIOU";
    const char* consoantes = "BCDFGHJKLMNPQRSTVWXYZ";
    const char* frase = argv[1];
    for (int chave_c=0; chave_c < 22; chave_c++) {
        for (int chave_v=0; chave_v < 5; chave_v++) {
            
            for (int i=0; i<strlen(frase); i++) { 
                int vogal_index = find_char(frase[i], vogais); 
                int consonant_index = find_char(frase[i], consoantes); 
                if (vogal_index != -1) { 
                    if (chave_v + vogal_index > 4) { 
                        printf("%c", vogais[vogal_index+chave_v-5]); 
                    } else { 
                        printf("%c", vogais[vogal_index+chave_v]); } 
                } else if (consonant_index != -1) { 
                    if (chave_c + consonant_index > 21) { 
                        printf("%c", consoantes[consonant_index+chave_c-21]); 
                    } else {
                        printf("%c", consoantes[consonant_index+chave_c]);
                    }
                } else {
                    printf("%c", frase[i]);
                }
            }
            printf("\n");

        }
        printf("%d ==========\n", chave_c);
    }
}

int find_char(char search,const char* arr) {
    int index = -1;
    for (int i=0; i<strlen(arr); i++) {
        if (search == arr[i]) {
            index = i;
            break;
        }
    }
    return index;
}
